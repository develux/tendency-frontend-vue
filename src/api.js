import {server} from './config';
import axios from 'axios'

const http = axios.create({
    baseURL: server,
});

export default http;